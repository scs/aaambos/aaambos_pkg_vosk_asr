# Vosk Asr

[API Docs](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos_pkg_vosk_asr)

> This is an [AAAMBOS](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos-website/) package. You can find more information about what AAAMBOS is [here](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos-website/docs/what_is_aaambos) and how to install it [here](https://scs.pages.ub.uni-bielefeld.de/aaambos/aaambos-website/docs/).

Speech Recognition with Vosk ASR.

## Install

You can clone the repo and do `pip install -e .`:
```bash
conda activate aaambos
sudo apt-get install libportaudio2
git clone https://gitlab.ub.uni-bielefeld.de/scs/aaambos/aaambos_pkg_vosk_asr.git
cd aaambos_pkg_vosk_asr
pip install -e .
```

If you cloned the simple flexdiam repo or aaambos repo you should want to remove the 2 requirements in the setup.py of the vosk ASR repo (It would unlink your cloned repos and would install the gitlab version in your site-packages).

The setup.py script should already **download** the the Vosk ASR models from [https://alphacephei.com/vosk/models](https://alphacephei.com/vosk/models) `vosk-model-small-de` and `vosk-model-spk` to `~/aaambos_agents/models/vosk/de`.
If it did not work you could try it with the script `python download_vosk_models.py` or do it by hand (download + extract) to `~/aaambos_agents/models/vosk/de`.

You can test Vosk ASR with the `vosk_microphone.py` script.

```bash
python vosk_microphone.py -m ~/aaambos_agents/models/vosk/de/vosk-model-small-de-0.15
```

## Running

For your `arch_config`:
```yaml
modules:
  vosk_asr:
    module_info: !name:vosk_asr.modules.vosk_asr.VoskAsrModule
```
Or for the simulator:
```yaml
modules:
  vosk_asr:
    module_info: !name:vosk_asr.modules.asr_simulator.AsrSimulator
    mean_frequency_step: 10
```
