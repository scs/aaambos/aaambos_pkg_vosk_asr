from __future__ import annotations

import asyncio
import os
import queue
import sys
import uuid
from typing import Type, Callable

import sounddevice as sd
from aaambos.core.module.extension import Extension
from aaambos.core.module.run import start_run_module
from aaambos.std.communications.attributes import MsgTopicSendingAttribute, MsgPayloadWrapperCallbackReceivingAttribute
from simple_flexdiam.modules.core.definitions import AsrResultsComFeatureOut, AsrResultsComFeatureIn, \
    AsrResults, AsrResultState, ASR_RESULTS
from vosk import Model, KaldiRecognizer

from aaambos.core.communication.service import CommunicationService
from aaambos.core.supervision.run_time_manager import ControlMsg, ControlType
from attrs import define

from aaambos.core.configuration.module_config import ModuleConfig
from aaambos.core.module.base import Module, ModuleInfo
from aaambos.core.module.feature import FeatureNecessity, Feature, SimpleFeatureNecessity


class VoskAsrModule(Module, ModuleInfo):
    config: VoskAsrConfig
    com: MsgTopicSendingAttribute | MsgPayloadWrapperCallbackReceivingAttribute | CommunicationService

    def __init__(self, config: ModuleConfig, com: CommunicationService, log, ext: dict[str, Extension], *args,
                 **kwargs):
        super().__init__(config, com, log, ext, *args, **kwargs)
        self.q = queue.Queue()
        self.do_shut_down = False

    @classmethod
    def provides_features(cls, config: ModuleConfig = ...) -> dict[Feature.name, tuple[Feature, FeatureNecessity]]:
        return {
            AsrResultsComFeatureOut.name: (AsrResultsComFeatureOut, SimpleFeatureNecessity.Required),
        }

    @classmethod
    def requires_features(cls, config: ModuleConfig = ...) -> dict[Feature.name, tuple[Feature, FeatureNecessity]]:
        return {
            AsrResultsComFeatureIn.name: (AsrResultsComFeatureIn, SimpleFeatureNecessity.Required),
        }

    @classmethod
    def get_module_config_class(cls) -> Type[ModuleConfig]:
        return VoskAsrConfig

    @classmethod
    def get_start_function(cls, config: ModuleConfig) -> Callable:
        return start_run_module

    async def initialize(self):
        pass

    async def step(self):
        pass

    async def run(self, control_queue, terminate_with_module):
        is_async_queue = isinstance(control_queue, asyncio.Queue)
        device_info = sd.query_devices(self.config.device, "input")
        # soundfile expects an int, sounddevice provides a float:
        samplerate = int(device_info["default_samplerate"])

        model = Model(os.path.expanduser(self.config.vosk_model_path))
        try:
            with sd.RawInputStream(samplerate=samplerate, blocksize=8000, device=self.config.device,
                                   dtype="int16", channels=1, callback=self.callback):

                rec = KaldiRecognizer(model, samplerate)
                rec.SetMaxAlternatives(10)
                # rec.SetWords(True)

                last_text = ""
                current_utterance_id = None
                while True:
                    if is_async_queue:
                        control_msg: ControlMsg = None if control_queue.empty() else await control_queue.get()
                    else:
                        control_msg: ControlMsg = None if control_queue.empty() else control_queue.get(block=False)

                    if control_msg:
                        match control_msg.control:
                            case ControlType.Terminate:
                                self.log.info(
                                    f"Received Terminate Control for {self.name}. Terminate. {terminate_with_module=}")
                                exit_code = self.terminate(control_msg)
                                if terminate_with_module:
                                    sys.exit(exit_code)
                                else:
                                    return exit_code
                    else:
                        data = self.q.get()
                        best = None
                        best_conf = 0.0
                        if rec.AcceptWaveform(data):
                            state = AsrResultState.FINAL
                            r = rec.Result()
                            raw = eval(r, {}, {})
                            if 'alternatives' in raw:
                                alts = raw['alternatives']
                                # print(alts)
                                if len(alts) > 0 and 'text' in alts[0] and alts[0]["text"] != "":
                                    best = alts[0]['text']
                                    best_conf = alts[0]['confidence']
                                    self.log.info(f"Final utterance: {best}")
                                    if current_utterance_id is None:
                                        current_utterance_id = str(uuid.uuid4())
                        else:
                            state = AsrResultState.HYPOTHESIS
                            r = rec.PartialResult()
                            if r.find('"partial" : ""') == -1:
                                partial = r.split('"')[3]
                                if partial != last_text:
                                    if current_utterance_id is None:
                                        current_utterance_id = str(uuid.uuid4())
                                    last_text = partial
                                    best = partial
                                    best_conf = 0.1

                        if best:
                            # best confidence > 1.0. Somehow just a large value
                            asr_result = AsrResults(
                                state=state,
                                source=self.name,
                                hypotheses=[],
                                confidences=[],
                                best=best,
                                best_confidence=best_conf,
                                utterance_id=current_utterance_id,
                            )
                            await self.com.send(self.tpc[ASR_RESULTS], asr_result)
                            if state == AsrResultState.FINAL:
                                current_utterance_id = None

                    if self.do_shut_down:
                        return
        except KeyboardInterrupt:
            self.terminate()
        finally:
            sys.exit(0)

    def terminate(self, control_msg: ControlMsg = None) -> int:
        exit_code = super().terminate()
        self.do_shut_down = True
        return exit_code

    def callback(self, indata, frames, time, status):
        try:
            """This is called (from a separate thread) for each audio block."""
            if status:
                print(status, file=sys.stderr)
            self.q.put(bytes(indata))
        except:
            sys.exit(0)

@define(kw_only=True)
class VoskAsrConfig(ModuleConfig):
    module_path: str = "vosk_asr.modules.vosk_asr"
    module_info: Type[ModuleInfo] = VoskAsrModule
    restart_after_failure: bool = True
    expected_start_up_time: float | int = 0
    mean_frequency_step: float = 20
    vosk_model_path: str = "~/aaambos_agents/models/vosk/de/vosk-model-small-de-0.15"
    device: None | str | int = 0


def provide_module():
    return VoskAsrModule
