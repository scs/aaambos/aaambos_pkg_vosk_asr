import os.path
import subprocess


def get_folder_name(link: str):
    return link.rsplit("/", 1)[1].rsplit(".", 1)[0]


def download_and_extract_vosk_models(destination: str = "~/aaambos_agents/models/vosk", language="de", model_links=..., speaker_model="https://alphacephei.com/vosk/models/vosk-model-spk-0.4.zip"):
    if model_links is ...:
        match language:
            case "de":
                model_links = ["https://alphacephei.com/vosk/models/vosk-model-small-de-0.15.zip", speaker_model]
            case "en":
                model_links = ["https://alphacephei.com/vosk/models/vosk-model-small-en-us-0.15.zip", speaker_model]
            case _:
                print(
                    f"\033[93m{language=} is not supported for downloading vosk models. Do specify model_links or download by hand.\033[0m")
                model_links = []
    if model_links:
        destination = f"{os.path.expanduser(destination)}/{language}"
        if not os.path.exists(destination):
            os.makedirs(destination)
        for model in model_links:
            folder_name = get_folder_name(model)
            if not os.path.exists(f"{destination}/{folder_name}"):
                print("--> Download vosk model", model)
                cmd = f"cd {destination} && curl -O {model} && unzip {destination}/{folder_name}.zip"
                print(cmd)
                subprocess.call(cmd, shell=True)


if __name__ == "__main__":
    download_and_extract_vosk_models()
