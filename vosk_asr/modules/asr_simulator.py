from __future__ import annotations

import uuid
import asyncio
from typing import Type

from aaambos.core.communication.service import CommunicationService
from aaambos.core.supervision.run_time_manager import ControlMsg
from aaambos.std.communications.attributes import MsgPayloadWrapperCallbackReceivingAttribute, MsgTopicSendingAttribute
from aaambos.std.guis.pysimplegui.window_extension import PySimpleGUIWindowExtension
from attrs import define

import PySimpleGUI as sg

from simple_flexdiam.modules.core.definitions import AsrResultsComFeatureOut, AsrResultsComFeatureIn, \
    AsrResults, AsrResultState, ASR_RESULTS

from aaambos.core.configuration.module_config import ModuleConfig
from aaambos.core.module.base import Module, ModuleInfo
from aaambos.core.module.feature import FeatureNecessity, Feature, SimpleFeatureNecessity
from aaambos.std.guis.pysimplegui.pysimplegui_window_ext import PySimpleGUIWindowExtensionFeature


class AsrSimulator(Module, ModuleInfo):
    config: AsrSimulatorConfig
    com: MsgTopicSendingAttribute | MsgPayloadWrapperCallbackReceivingAttribute | CommunicationService

    def __init__(self, config: ModuleConfig, com, log, ext, gui_window: PySimpleGUIWindowExtension, *args, **kwargs):
        super().__init__(config, com, log, ext, *args, **kwargs)
        self.gui_window = gui_window
        self.last_input = ""

    @classmethod
    def provides_features(cls, config: ModuleConfig = ...) -> dict[Feature.name, tuple[Feature, FeatureNecessity]]:
        return {
            AsrResultsComFeatureOut.name: (AsrResultsComFeatureOut, SimpleFeatureNecessity.Required),
        }

    @classmethod
    def requires_features(cls, config: ModuleConfig = ...) -> dict[Feature.name, tuple[Feature, FeatureNecessity]]:
        return {
            PySimpleGUIWindowExtensionFeature.name: (PySimpleGUIWindowExtensionFeature, SimpleFeatureNecessity.Required),
            AsrResultsComFeatureIn.name: (AsrResultsComFeatureIn, SimpleFeatureNecessity.Required),
        }

    @classmethod
    def get_module_config_class(cls) -> Type[ModuleConfig]:
        return AsrSimulatorConfig

    async def initialize(self):
        await asyncio.sleep(1)
        self.gui_window.setup_window(window_title="ASR Simulator", layout=self.create_layout())
        self.gui_window.set_event_handler(self.handle_window_event)

    def create_layout(self) -> list:
        return [
            [
                sg.Text("Utterance"),
                sg.InputText("", key="payload_input_text", enable_events=True),
                sg.Submit("Say", key="send_msg")
            ],
            [
                sg.Checkbox("Incremental", key='incr', default=True)
            ],
        ]

    async def handle_window_event(self, event, values):
        if event == "send_msg":
            asr_result = AsrResults(
                state=AsrResultState.FINAL,
                source=self.name,
                hypotheses=[],
                confidences=[],
                best=values["payload_input_text"],
                best_confidence=1.0,
                utterance_id=str(uuid.uuid4()),
            )
            await self.com.send(self.tpc[ASR_RESULTS], asr_result)
            self.gui_window.window["payload_input_text"].update(value="")
        elif values["incr"] and event == "payload_input_text" and values["payload_input_text"].endswith(" "):
            self.last_input = values["payload_input_text"]
            asr_result = AsrResults(
                state=AsrResultState.HYPOTHESIS,
                source=self.name,
                hypotheses=[],
                confidences=[],
                best=values["payload_input_text"],
                best_confidence=1.0,
                utterance_id=str(uuid.uuid4()),
            )
            await self.com.send(self.tpc[ASR_RESULTS], asr_result)

    async def step(self):
        pass

    def terminate(self, control_msg: ControlMsg = None) -> int:
        exit_code = super().terminate()
        return exit_code


@define(kw_only=True)
class AsrSimulatorConfig(ModuleConfig):
    module_path: str = "vosk_asr.modules.asr_simulator"
    module_info: Type[ModuleInfo] = AsrSimulator
    restart_after_failure: bool = True
    expected_start_up_time: float | int = 0


def provide_module():
    return AsrSimulator
