#!/usr/bin/env python

"""The setup script."""

from setuptools import setup, find_packages

with open('README.md') as readme_file:
    readme = readme_file.read()

with open('CHANGELOG.md') as history_file:
    history = history_file.read()

requirements = [
    "aaambos @ git+https://gitlab.ub.uni-bielefeld.de/scs/aaambos/aaambos@main",
    "simple_flexdiam @ git+https://gitlab.ub.uni-bielefeld.de/scs/aaambos/aaambos_pkg_simple_flexdiam@main",
    "sounddevice",
    "vosk"
    ]

test_requirements = ['pytest>=3', ]

setup(
    author="Florian Schröder",
    author_email='florian.schroeder@uni-bielefeld.de',
    python_requires='>=3.10',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.10',
    ],
    description="Speech Recognition with Vosk ASR.",
    install_requires=requirements,
    license="MIT license",
    long_description=readme + '\n\n' + history,
    include_package_data=True,
    keywords=['aaambos', 'vosk_asr'],
    name='vosk_asr',
    packages=find_packages(include=['vosk_asr', 'vosk_asr.*']),
    test_suite='tests',
    tests_require=test_requirements,
    url='https://gitlab.ub.uni-bielefeld.de/scs/aaambos/aaambos_pkg_vosk_asr',
    version='0.1.3',
    zip_safe=False,
)

try:
    from download_vosk_models import download_and_extract_vosk_models
    download_and_extract_vosk_models()
except:
    print(f"\033[93mDownloading and extracting the vosk speech models failed.\nDo it by hand and download them.\033[0m")